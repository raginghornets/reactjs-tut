import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square 
        value={this.props.squares[i]} 
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render = () => { 
    let rows = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]
    return (rows.map(
      row => 
        <div className="board-row">
          {row.map(i => this.renderSquare(i))}
        </div>
    ));
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        index: null,
        player: null,
      }],
      stepNumber: 0,
      xIsNext: true,
      isDescending: true,
    }

    this.toggleOrder = this.toggleOrder.bind(this);
  }

  nextPlayer = () => this.state.xIsNext ? 'X' : 'O';

  getPosition(i) {
    const positions = [
      [0, 0], [1, 0], [2, 0],
      [0, 1], [1, 1], [2, 1],
      [0, 2], [1, 2], [2, 2]
    ]
    return positions[i];
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    
    if (getWinnerInfo(squares).player || squares[i]) {
      return;
    }
    
    squares[i] = this.nextPlayer();
    this.setState({
      history: history.concat([{
        squares: squares,
        index: i,
        player: squares[i],
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
    for (let i = 0; i < 9; i++) {
      document.getElementsByClassName('square')[i].setAttribute('class', 'square');
    }
  }

  toggleOrder() {
    this.setState({
      isDescending: !this.state.isDescending
    });
  }
  
  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = getWinnerInfo(current.squares);

    const moves = history.map((step, move) => {
      const desc = move ?
        'Go to move #' + move + ' ' +
          '(' + step.player + ' @ ' +
          this.getPosition(step.index) + ')' :
        'Go to game start';

      // Bold the currently selected step in the history list
      let name = step === current ? 'currently-selected' : 'step';
    
      return (
        <li key={move}>
          <button
            className={name}
            onClick={() => this.jumpTo(move)}>
            {desc}
          </button>
        </li>
      );
    });

    let status = () => {
      if (winner.player) {
        for (let i = 0; i < 9; i++) {
          for (let j = 0; j < 3; j++) {
            if (i === winner.line[j]) {
              console.log(this.state.stepNumber);
              document.getElementsByClassName('square')[i].setAttribute('class', 'square winning-square');
            }
          }
        }
        return 'Winner: ' + winner.player;
      } else {
        if (this.state.stepNumber === 9) {
          return 'Draw!'
        } else {
          return 'Next player: ' + this.nextPlayer();          
        }
      }
    }

    let movesClassName = 'moves';

    if (!this.state.isDescending) {
      movesClassName += ' reversed';
    }

    return (
      <div>
        <h1>Tic Tac Toe</h1>      
        <div className="game">
          <div className="game-board">
            <Board 
              squares={current.squares}
              onClick={(i) => this.handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status()}</div>
            <button 
              id="toggleOrder" 
              onClick={this.toggleOrder}>
              Toggle order
            </button>
            <ol className={movesClassName}>{moves}</ol>
          </div>
        </div>
      </div>
    );
  }
}

function getWinnerInfo(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (
      squares[a] && squares[a] === squares[b] && 
      squares[a] === squares[c]) {
        return ({
          player: squares[a],
          line: lines[i],
        });
    }
  }
  return ({
    player: null,
    line: null,
  });
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);